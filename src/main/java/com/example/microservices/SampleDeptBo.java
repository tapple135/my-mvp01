package com.example.microservices;

import lombok.Data;

@Data
public class SampleDeptBo {
	 private int  deptNo 		; // 부서코드
	 private String  deptName 		; // 부서명
}
