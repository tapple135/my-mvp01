package com.example.microservices.web;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.microservices.Hello;
import com.example.microservices.SampleDeptBo;
import com.example.microservices.SampleDeptDao;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Api(value="Hello Service API")
@RestController
public class HelloController {
	@Autowired
	private SampleDeptDao sampleDeptDao;
	
	private String msg = "%s님 반갑습니다.";
	private final AtomicLong vistorConouter = new AtomicLong();
	
	@ApiOperation(value="hello api 2222")
	@RequestMapping(value="/hello", method=RequestMethod.GET)
	public Hello getHelloMsg(@RequestParam(value="name") String name) {
		return new Hello(vistorConouter.incrementAndGet(),String.format(msg, name));
	}

	@ApiOperation(value="부서 정보 가져오기  ")
	@RequestMapping(value="/depts", method=RequestMethod.GET)
	public ResponseEntity <List<SampleDeptBo>> getDeptList() { 
		
		List<SampleDeptBo> list = null;
		try {
			log.info("Start db select");
			list = sampleDeptDao.selectDept();
		} catch (Exception e) {
			e.printStackTrace();
		}
		log.info("dept counts :"+list.get(0).getDeptName());
		log.info("dept counts :"+list.size());
		
		return new ResponseEntity<List<SampleDeptBo>> (list, HttpStatus.OK);
	}

	
	@ApiOperation(value="부서 정보 가져오기  DB ")
	@RequestMapping(value="/db/test", method=RequestMethod.GET)
	public ResponseEntity <String> getUserListTest() { 
		
		int test = -1;
		try {
			log.info("Start db select");
			test = sampleDeptDao.selectTest();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		log.debug("user counts :"+test);
		
		return new ResponseEntity<String> (test+"", HttpStatus.OK);
	}	
	
	@ApiOperation(value="부서 정보 등록하기 ")
	@RequestMapping(value="/depts", method=RequestMethod.POST)
	public ResponseEntity <String > setUserInsert(
			@RequestBody SampleDeptBo sampleDeptBo
		) throws Exception { 
		
		List<SampleDeptBo> list = null;
		log.info("Start db insert");
		int re  = sampleDeptDao.insertDept(sampleDeptBo);
		log.debug("result :"+ re);
		
		return new ResponseEntity<String> (re+"", HttpStatus.OK);
	}
}
