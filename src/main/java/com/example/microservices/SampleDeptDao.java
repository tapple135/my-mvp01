package com.example.microservices;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;



@Mapper
public interface SampleDeptDao {
	
	/**
	 * 부서 전체 정보 가져오기 
	 * @return
	 * @throws Exception
	 */
	List<SampleDeptBo> selectDept() throws Exception;	

	int selectTest() throws Exception;
	
	
	/**
	 * 사용자 등록하기 
	 * @param sampleDeptBo
	 * @return
	 * @throws Exception
	 */
	int insertDept(SampleDeptBo sampleDeptBo) throws Exception;
}
