package com.example.microservices;

import lombok.Data;

@Data
public class Hello {

	
	private long count;
	private String msg;
	

	
	public Hello(long count, String msg) {
		super();
		this.count = count;
		this.msg = msg;
	}


}
