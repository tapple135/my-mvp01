package com.example.microservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyMvp01Application {

	public static void main(String[] args) {
		SpringApplication.run(MyMvp01Application.class, args);
	}

}
